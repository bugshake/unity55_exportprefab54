﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using Bugshake.Json;

public class ExportPrefab : Editor
{
    static string indentstr = "                                               ";

    //[MenuItem("GameObject/Export/Traverse Selected Objects")]
    public static void Traverse()
    {
        var gos = Selection.gameObjects;
        foreach (var go in gos)
        {
            Export(go, 0);
        }
    }

    //[MenuItem("GameObject/Export/Find Selected refab")]
    public static void FindPrefab()
    {
        var selectedPrefabPaths = prefabsFromSelection();
        foreach (var path in selectedPrefabPaths)
        {
            Debug.Log(path);
        }
    }

    //[MenuItem("GameObject/Export/Parse Selected Prefabs")]
    public static void ParsePrefab()
    {
        var selectedPrefabPaths = prefabsFromSelection();
        foreach (var path in selectedPrefabPaths)
        {
            string s = File.ReadAllText(path);
            var result = Yaml.Parse(s);
            Debug.Log(JSON.Serialize(result, true));
        }
    }

    [MenuItem("Assets/Export/Unity 5.4 prefab", false, 0)]
    public static void ConvertPrefab()
    {
        string outputRoot = EditorUtility.OpenFolderPanel("Choose new Assets root (path structure is preserved)", Application.streamingAssetsPath, "");

        var selectedPrefabPaths = prefabsFromSelection();
        foreach (var path in selectedPrefabPaths)
        {
            string s = File.ReadAllText(path);
            var result = Yaml.Parse(s);
            string conv = Yaml.ToPrefab_v4(result);
            //string outputPath = path.Replace("Assets/CustomExport/", "D:\\Dev\\Unity\\GIT\\EndlessLandscape\\Assets\\JoramEdit\\").Replace(".prefab", "_auto.prefab");
            string outputPath = path.Replace("Assets/", outputRoot + "/");//.Replace(".prefab", "_55to54.prefab");
            if (File.Exists(outputPath))
            {
                Debug.LogWarningFormat("File {0} overwritten", outputPath);
            }
            else
            {
                var dirOnly = Path.GetDirectoryName(outputPath);
                Debug.Log(dirOnly);
                Directory.CreateDirectory(dirOnly);
            }
            File.WriteAllText(outputPath, conv);
            Debug.LogFormat("Converted {0} to {1}", path, outputPath);
        }
    }

    static HashSet<string> prefabsFromSelection()
    {
        var selectedPrefabPaths = new HashSet<string>();
        {
            var gos = Selection.gameObjects;
            foreach (var go in gos)
            {
                if (PrefabUtility.GetPrefabType(go) == PrefabType.Prefab)
                {
                    selectedPrefabPaths.Add(AssetDatabase.GetAssetOrScenePath(go));
                }
                else
                {
                    var pgo = PrefabUtility.GetPrefabParent(go);
                    if (pgo != null)
                    {
                        selectedPrefabPaths.Add(AssetDatabase.GetAssetOrScenePath(pgo));
                    }
                }
            }
        }
        return selectedPrefabPaths;
    }

    static void parsePrefab(GameObject go)
    {
        string path = AssetDatabase.GetAssetPath(go);
        Debug.Log(path);
    }

    static void Export(GameObject go, int depth)
    {
        output(depth, "GameObject {{");
        output(depth + 1, "name: {0}", go.name);
        output(depth + 1, "layer: {0}", go.layer);
        output(depth + 1, "active: {0}", go.activeSelf);
        output(depth + 1, "components: [");
        var comps = go.GetComponents<Component>();
        for (int i = 0; i < comps.Length; ++i)
        {
            if (comps[i] is Transform)
            {
                Export(comps[i] as Transform, depth + 2);
            }
            else
            {
                output(depth + 2, comps[i].GetType().Name);
            }
        }
        output(depth + 1, "]");
        output(depth, "}}");
    }

    static void Export(Transform t, int depth)
    {
        if (t == null) return;
        output(depth, "Transform {{");
        output(depth + 1, "scale.x: {0}", t.localScale.x);
        output(depth + 1, "children: [");
        for (int i = 0; i < t.childCount; ++i)
        {
            Export(t.GetChild(i).gameObject, depth + 2);
        }
        output(depth, "}}");
    }

    static void output(int indent, string fmt, params object[] args)
    {
        string s = indentstr.Substring(0, indent * 2) + string.Format(fmt, args);
        System.Console.WriteLine(s);
    }
}