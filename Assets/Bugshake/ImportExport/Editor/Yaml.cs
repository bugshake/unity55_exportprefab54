﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;

public class Yaml
{
    public static object Parse(string s)
    {
        return new Yaml().ParseUnityPrefab(s);
    }

    public static string ToPrefab_v4(object o)
    {
        return new Yaml().SerializePrefab(o, "{0}", "4");
    }

    public static string ToPrefab_v5(object o)
    {
        return new Yaml().SerializePrefab(o, "component", "5");
    }

    Dictionary<long, int> fileID2index;
    Node currentNode = null;

    string SerializePrefab(object o, string componentFmt, string versionString)
    {
        object[] a = (object[])o;

        fileID2index = new Dictionary<long, int>();
        foreach (var el in a)
        {
            var node = el as Node;
            if (node != null)
            {
                fileID2index[node.fileID] = node.index;
            }
        }
        
        StringBuilder sb = new StringBuilder();
        for (int index = 0; index < a.Length; ++index)
        {
            outputPrefab(sb, a[index] as Skipped);
            outputPrefab(sb, a[index] as Value, componentFmt, versionString);
            outputPrefab(sb, a[index] as Node);
        }
        return sb.ToString();
    }

    int findIndexFileID(string s)
    {
        // {fileID: 1000010029491498}
        Debug.Assert(s.StartsWith("{fileID: "));
        s = s.Substring("{fileID: ".Length);
        s = s.Substring(0, s.IndexOf("}"));
        long fileID = long.Parse(s);
        return fileID2index[fileID];
    }

    void outputPrefab(StringBuilder sb, Skipped o)
    {
        if (o == null) { return; }
        sb.AppendLine(o.line);
    }

    void outputPrefab(StringBuilder sb, Value o, string componentFmt, string versionString)
    {
        if (o == null) { return; }

        sb.Append("  ");
        sb.Append(o.name);
        sb.Append(':');
        if (o.value is string)
        {
            if (o.name == "serializedVersion" && currentNode != null && currentNode.type == "GameObject")
            {
                o.value = versionString;
            }
            sb.Append(' ');
            sb.Append((string)o.value);
        }
        else
        {
            object[] a = (object[])o.value;
            foreach (var child in a)
            {
                var val = child as Value;
                if (o.name == "m_Component")
                {
                    val.name = string.Format(componentFmt, findIndexFileID((string) val.value));
                }
                sb.AppendFormat("\n  - {0}: {1}", val.name, val.value);
            }
        }
        sb.Append('\n');
    }

    void outputPrefab(StringBuilder sb, Node o)
    {
        if (o == null) { return; }
        currentNode = o;
        sb.AppendFormat("--- !u!{0} &{1}\n{2}:\n", o.index, o.fileID, o.type);
    }

    object ParseUnityPrefab(string fileContents)
    {
        List<object> result = new List<object>();
        string[] lines = fileContents.Split('\r', '\n');
        for( int index = 0; index < lines.Length;++index)
        {
            var line = lines[index];
            if (line.StartsWith("--- !u!"))
            {
                // --- !u!1 &1851658820713782
                line = line.Substring("--- !u!".Length);
                int idx = line.IndexOf(" &");
                var s1 = line.Substring(0, idx);
                var s2 = line.Substring(idx + 2);

                ++index;
                line = lines[index];
                string type = line.TrimEnd(':');

                result.Add(new Node
                {
                    index = int.Parse(s1),
                    fileID = long.Parse(s2),
                    type = type
                });
            }
            else if (line.StartsWith("  - "))
            {
                // child
                result.Add(new Skipped { line = line });
            }
            else if (line.StartsWith(" "))
            {
                // key value
                var keyValue = parseKeyValue(line.Substring(2));
                if (keyValue.name == "m_Component")
                {
                    var list = new List<object>();
                    while (++index < lines.Length)
                    {
                        string subline = lines[index];
                        if (subline.StartsWith("  - "))
                        {
                            list.Add(parseKeyValue(subline.Substring(4)));
                        }
                        else
                        {
                            --index;
                            break;
                        }
                    }
                    keyValue.value = list.ToArray();
                }
                //else if (keyValue.name == "m_Children" && !string.IsNullOrEmpty((string) keyValue.value))
                //{
                //    var list = new List<object>();
                //    while (++index < lines.Length)
                //    {
                //        string subline = lines[index];
                //        if (subline.StartsWith("  - "))
                //        {
                //            //list.Add(subline.Substring(4)); // {fileID: 4000014214925170}
                //        }
                //        else
                //        {
                //            --index;
                //            break;
                //        }
                //    }
                //    keyValue.value = list.ToArray();
                //}
                result.Add(keyValue);
            }
            else
            {
                // header?
                result.Add(new Skipped { line = line });
            }
        }
        return result.ToArray();
    }

    Value parseKeyValue(string s)
    {
        int pos1 = s.IndexOf(':');
        if (pos1 > 0)
        {
            string name = s.Substring(0, pos1);
            string value = pos1 < s.Length - 2 ? s.Substring(pos1 + 2) : "";
            return new Value
            {
                name = name,
                value = value
            };
        }
        return null;
    }

    public class Skipped
    {
        public string line;
    }

    public class Node
    {
        public int index;
        public long fileID;
        public string type;
    }

    public class Value
    {
        public string name;
        public object value;
    }
}