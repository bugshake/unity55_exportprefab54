# README #

Starting unity version 5.5, the file format for prefabs and scenes has changed a bit. Because of this, you cannot use them in projects that use an older version of Unity.

### What is this repository for? ###

* Downgrade your Unity 5.5+ prefabs to the Unity 5.4 file format.
* Version 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Set asset serialization to text-only
* Copy clases into your Unity 5.5+ project
* (Multiple) Select prefabs you want to export
* Select output dir (typically Assets dir of 5.4- project)
